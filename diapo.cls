% Classe pour les diaporamas
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\PassOptionsToPackage{svgnames, dvipsnames}{xcolor}

\LoadClass[aspectratio=169, 10pt, handout]{beamer}

\RequirePackage[french]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage[math]{kurier}
\RequirePackage{microtype}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{amssymb}
\RequirePackage{mathtools}
\RequirePackage{xspace}
\RequirePackage{tikz}
\RequirePackage{tikz-cd}
\RequirePackage{mathrsfs}
\RequirePackage{stmaryrd}
% \RequirePackage{enumitem}

\newcounter{chapter}
\newskip\EspacementPar

\DeclareSymbolFont{extrasymbols}{OMS}{cmsy}{m}{n}
\DeclareMathDelimiter{\lVert}
  {\mathopen}{extrasymbols}{"6B}{largesymbols}{"0D}
\DeclareMathDelimiter{\rVert}
  {\mathclose}{extrasymbols}{"6B}{largesymbols}{"0D}

\DeclareFontFamily{U}{futm}{}
\DeclareFontShape{U}{futm}{m}{n}{
  <-> s * [.92] fourier-bb
  }{}
\DeclareSymbolFont{Ufutm}{U}{futm}{m}{n}
\DeclareSymbolFontAlphabet{\mathbb}{Ufutm}

\setbeamertemplate{caption}[numbered]

\definecolor{ens}{HTML}{3B4CA7}
\setbeamercolor{structure}{fg=ens}
\newcommand{\pagesection}{\clearpage\null\vfill\sectionpage\vfill\null\clearpage}
\renewcommand{\sectionname}{Partie}
\setbeamertemplate{section in toc}[sections numbered]
\setbeamertemplate{subsection in toc}[subsections numbered]

\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{footline}{
	\setbeamercolor{pied}{fg=white, bg=ens}
	\begin{beamercolorbox}[ht=3.5ex, leftskip=.2cm, rightskip=.2cm, sep=.04cm]{pied}
		\insertshortauthor\hfill\insertframenumber/\inserttotalframenumber\hfill\inserttitle
	\end{beamercolorbox}
}

\setbeamertemplate{section in toc}
	{\bfseries\inserttocsectionnumber\enskip\inserttocsection\par}
\setbeamertemplate{subsection in toc}
	{\leavevmode\leftskip=2em\inserttocsectionnumber.\inserttocsubsectionnumber\enskip\inserttocsubsection\par}

\defbeamertemplate*{title page}{customized}[1][]{
	\centering
	\vskip3em
	\Large\textcolor{structure}{\bfseries\inserttitle}
	\vskip.5em
	\usebeamerfont{subtitle}\insertsubtitle
	\vskip2em
	{\footnotesize\insertinstitute}\par
	\vskip2em
	{\usebeamerfont{author}\insertauthor\par}\smallbreak
	\footnotesize\usebeamerfont{date}\insertdate
	\vskip3em\hfill\inserttitlegraphic\hfill\null
}

\setbeamerfont{block title}{series=\bfseries}

\AtBeginDocument{
	\abovedisplayskip=7pt plus 5pt minus 5pt
	\belowdisplayskip=7pt plus 5pt minus 5pt
}

\setbeamercolor{frametitle}{fg=white, bg=ens}
\setbeamertemplate{frametitle}{%
	\vskip-3pt
	\begin{beamercolorbox}[sep=0.6em, wd=\paperwidth, ht=1.9em, leftskip=0cm, rightskip=0cm]{frametitle}%
		\par
		\rule[-0.6ex]{0pt}{2.5ex}\insertframetitle\hfill\tiny\normalfont%
		\raisebox{2pt}{\begin{tabular}{r}
			\ifnum0<\value{section}\bfseries\thesection. \insertsection\fi \\
			\ifnum0<\value{subsection}\thesection.\thesubsection. \insertsubsection\fi\strut
		\end{tabular}}%
		\strut
	\end{beamercolorbox}%
}

\tikzset{ampersand replacement=\&, >=Straight Barb}
\setbeamertemplate{itemize subitem}[square]

{\catcode`"=\active
 \gdef"#1{_\text{#1}}}
\mathcode`"="8000

\setbeamertemplate{bibliography item}{$\blacktriangleright$}
\usepackage{csquotes}
% \usepackage[backend=biber, style=math-numeric]{biblatex}
% \renewcommand*{\bibfont}{\footnotesize}

% --- Blocs ---

\RequirePackage{tcolorbox}

\newtcolorbox{myblock}[3][]{
	left*=0mm,
	right*=0mm,
	grow to left by=3mm,
	grow to right by=3mm,
	toptitle=1mm,
	rightrule=-1mm,
	bottomrule=-1mm,
	colframe=#2,
	colback=white,
	before skip=\bigskipamount, after skip=\bigskipamount,
	title={\bfseries#3. ---},
	before upper={\parindent0pt},
	#1
}

\colorlet{sec}{ens}

\newenvironment{encadre}[1]{
	\setbeamertemplate{itemize item}{\color{sec}$\blacktriangleright$}
	\begin{myblock}{sec}{#1}
}{
	\end{myblock}
}

% --- Pauses ---

\let\save@measuring@true\measuring@true
\def\measuring@true{%
    \save@measuring@true
    \def\beamer@sortzero##1{\beamer@ifnextcharospec{\beamer@sortzeroread{##1}}{}}%
    \def\beamer@sortzeroread##1<##2>{}%
    \def\beamer@finalnospec{}%
}

\RequirePackage{array}
\newcolumntype{R}[1]{>{\onslide<#1->}r<{\onslide}}

% --- Virgules ---

\AtBeginDocument{%
    \mathchardef\mathcomma\mathcode`\,
    \mathcode`\,="8000
}

{\catcode`,=13
    \gdef,{\futurelet\@let@token\sm@rtcomma}
}

\def\sm@rtcomma{%
    \ifx\@let@token\@sptoken\else%
    \ifx\@let@token\mathpunct\else%
    \mathord\fi\fi\mathcomma%
}

% --- Commandes ---

\let\definition\relax
% \let\alt\relax
\RequirePackage{commandes}

\endinput
